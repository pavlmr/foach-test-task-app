import React, { FC, useEffect} from 'react';
import { Grid } from '@material-ui/core';
import { Item } from './';
import { useSelector } from "react-redux";
import iState from '../../interfaces/State.interface';

const Items: FC = () => {
    const users = useSelector((state: iState) => state.data.users);

    if (!users.length) return <Grid container>Загрузка</Grid>
    
    return (
        <Grid container component="ul">
            {users && users.map((item, index) => <Item key={index} avatar={item.avatar} name={item.name} position={item.position} vacation={item.vacation} /> )}
        </Grid>
    )
}

export default Items