import React, {FC} from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles, createStyles, Theme, withStyles } from '@material-ui/core/styles';
import { CardMedia, Switch, FormControlLabel } from '@material-ui/core';
import iUser from '../../interfaces/User.interface';
import clsx from 'clsx';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        card: {
            backgroundColor: '#FFFFFF',
            boxShadow: 'none',
            border: '1px solid #1EAAFC',
            borderRadius: '5px',
            display: 'flex',
            flexWrap: 'nowrap',
            paddingTop: '32px',
            paddingBottom: '32px',
            marginTop: '22px',
            marginBottom: '22px'
        },
        media: {
            height: '162px',
            width: '162px',
        },
        content: {
            paddingLeft: '46px',
            paddingRight: '32px',
            paddingBottom: '29px'
        },
        text: {
            fontFamily: 'Lato',
            fontSize: '30px',
            lineHeight: 'auto',
            letterSpacing: '0.75px',
        },
        name:{
            color: '#909090',
            marginBottom: '36px',
        },
        position: {
            color: '#747474',
            marginBottom: '10px',
        }
    })
)

const MySwitch = withStyles({
    root: {
        height: '36px',
        padding: '11px',
        width: '54px'
    },
    track: {
        backgroundColor: '#979797',
        opacity: '30%'
    },
    switchBase: {
        color: '#979797',
        boxShadow: 'none'
    },
    thumb: {
        width: '18px',
        height: '18px',
        boxShadow: 'none'
    }
})(Switch);

const MyFormControlLabel = withStyles({
    label : {
        color: '#747474',
        paddingLeft: '12px'
    }
})(FormControlLabel);

interface State {
    vacation: boolean;
}

const Item: FC<iUser> = ({ name, avatar, position, vacation}) => {
    const [state, setState] = React.useState<State>({
        vacation: vacation
    })
    const classes = useStyles();

    const handleChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
        setState({ ...state, [name]: event.target.checked });
    };

    return (
        <Grid item xs={12} className={classes.card} container component="li" >
            <Grid item xs={3} justify="center" direction="row" alignItems="center" container>
                <CardMedia
                    className={classes.media}
                    image={avatar}
                    title="Avatar"
                />
            </Grid>
            <Grid item xs={9} className={classes.content} container direction="column" justify="center">
                <Typography gutterBottom variant="h5" component="h2" className={clsx(classes.name, classes.text)}>{name}</Typography>
                <Typography variant="body2" color="textSecondary" component="p" className={clsx(classes.position, classes.text)}>
                    {position}
                </Typography>
                <MyFormControlLabel
                    control={
                        <MySwitch
                            checked={state.vacation}
                            onChange={handleChange('vacation')}
                            value="vacation"
                            color="primary"
                        />
                    }
                    label="On vacation"
                />
            </Grid>
        </Grid>
)}

export default Item;