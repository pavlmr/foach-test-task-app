import React, { FC } from 'react';
import { Grid, MenuItem, Button, Typography, Box } from '@material-ui/core';
import { TextField, FormControlLabel, Checkbox, FormControl, InputLabel, InputAdornment, IconButton, OutlinedInput, FormHelperText  } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import validate from 'validate.js';
import { amountToInvest, constraints, formInfo } from '../data';
import iState from '../interfaces/FormState.interface';
import iValues from '../interfaces/FormValues.interface';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: '#ffffff',
            maxWidth: '470px',
            fontSize: '14px',
            padding: '42px'
        },
        h1: {
            fontFamily: 'Playfair Display',
            fontSize: '25px',
            lineHeight: '33px',
            paddingLeft: '5px',
            paddingRight: '5px',
            marginTop: '15px',
            marginBottom: '47px',
            position: 'relative'
        },
        textField: {
            paddingLeft: '5px',
            paddingRight: '5px',
            marginTop: '0px',
            marginBottom: '15px'
            
        },
        controlLabel: {
            paddingLeft: '5px',
            paddingRight: '5px',
            alignItems: 'flex-start',
            paddingTop: '4px'
        },
        btn: {
            marginTop: '42px',
            marginBottom: '53px',
            fontFamily: 'Lato',
            fontWeight: 'bold'
        },
        visIcon: {
            color: '#EFEFEF'
        },
        line: {
            width: '48px',
            height: '3px',
            backgroundColor: '#3AB047',
            position: 'absolute',
            top: '-15px'
        }
    }),
);

const Form: FC = () => {
    const classes = useStyles();
    const [values, setValues] = React.useState<iState>({
        firstName: '',
        lastName: '',
        phone: '',
        amountInvest: '',
        email: '',
        confEmail: '',
        password: '',
        confPassword: '',
        terms: '',
        accept: '',
        showPassword: false,
        errors: null
    });

    const errors = validate({
        firstName: values.firstName,
        lastName: values.lastName,
        phone: values.phone,
        amountInvest: values.amountInvest,
        email: values.email,
        confEmail: values.confEmail,
        password: values.password,
        confPassword: values.confPassword,
        terms: values.terms,
        accept: values.accept,
    }, {
        firstName: constraints.firstName,
        lastName: constraints.lastName,
        phone: constraints.phone,
        amountInvest: constraints.amountInvest,
        email: constraints.email,
        confEmail: constraints.confEmail,
        password: constraints.password,
        confPassword: constraints.confPassword,
        terms: constraints.terms,
        accept: constraints.accept
    });

    const handleChange = (prop: keyof iValues) => (event: React.ChangeEvent<HTMLInputElement>) => {
        let error:any = undefined

        if (prop === 'confPassword'){
            error = validate({ [prop]: event.target.value, password: values.password  }, { [prop]: constraints[prop] });
        } else if (prop === 'confEmail'){
            error = validate({ [prop]: event.target.value, email: values.email }, { [prop]: constraints[prop] });
        } else {
            error = validate({ [prop]: event.target.value }, { [prop]: constraints[prop] });
        }
        
        const erorrs:any = {
            ...values.errors,
            ...error
        };

        !error && delete erorrs[prop];

        setValues({
            ...values,
            [prop]: event.target.value,
            errors: {...erorrs}
        });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    let history = useHistory();

    const handleSubmitBtn = () => {
        errors && setValues({
            ...values,
            errors: { ...errors }
        });
        !errors && history.push("/users");
    }
    
    return (
        <form className={classes.root}>
            <Grid container direction="row" >
                <Grid item xs={12}>
                    <Typography variant='h1' className={classes.h1}><Box component="span" className={classes.line}></Box>Sign up</Typography>
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="firstName"
                        value={values.firstName}
                        onChange={handleChange('firstName')}
                        error={!!(values.errors && values.errors.firstName)}
                        helperText={(values.errors && values.errors.firstName) ? values.errors.firstName[0] : ''}
                        label="First Name"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        required
                        fullWidth
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="lastName"
                        value={values.lastName}
                        onChange={handleChange('lastName')}
                        error={!!(values.errors && values.errors.lastName)}
                        helperText={(values.errors && values.errors.lastName) ? values.errors.lastName[0] : ''}
                        label="Last Name"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        required
                        fullWidth
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        id="phone"
                        value={values.phone}
                        onChange={handleChange('phone')}
                        error={!!(values.errors && values.errors.phone)}
                        helperText={(values.errors && values.errors.phone) ? values.errors.phone[0] : ''}
                        label="Phone Number"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        required
                        fullWidth
                    />
                </Grid>
                
                <Grid item xs={6}>
                    <TextField
                        id="amountInvest"
                        value={values.amountInvest}
                        onChange={handleChange('amountInvest')}
                        error={!!(values.errors && values.errors.amountInvest)}
                        helperText={(values.errors && values.errors.amountInvest) ? values.errors.amountInvest[0] : ''}
                        label="Amount to invest"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        select
                        required
                        fullWidth>
                        {amountToInvest.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                
                <Grid item xs={12}>
                    <TextField
                        id="email"
                        value={values.email}
                        onChange={handleChange('email')}
                        error={!!(values.errors && values.errors.email)}
                        helperText={(values.errors && values.errors.email) ? values.errors.email[0] : ''}
                        label="Email Address"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        fullWidth
                        required
                    />
                </Grid>

                <Grid item xs={12}>
                    <TextField
                        id="confEmail"
                        value={values.confEmail}
                        onChange={handleChange('confEmail')}
                        error={!!(values.errors && values.errors.confEmail)}
                        helperText={(values.errors && values.errors.confEmail) ? values.errors.confEmail[0] : ''}
                        label="Confirm Email Address"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                        required
                        fullWidth
                    />
                </Grid>

                <Grid item xs={12}>
                    <FormControl className={classes.textField} fullWidth required variant="outlined" error={!!(values.errors && values.errors.password)}>
                        <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                        <OutlinedInput
                            id="password"
                            type={values.showPassword ? 'text' : 'password'}
                            value={values.password}
                            onChange={handleChange('password')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        className={classes.visIcon}
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            labelWidth={70}
                        />
                        {values.errors && values.errors.password && <FormHelperText>{(values.errors && values.errors.password) ? values.errors.password[0] : ''}</FormHelperText>}
                    </FormControl>
                </Grid>

                <Grid item xs={12}>
                    <FormControl className={classes.textField} fullWidth required variant="outlined" error={!!(values.errors && values.errors.confPassword)}>
                        <InputLabel htmlFor="outlined-adornment-password">Confirm Password</InputLabel>
                        <OutlinedInput
                            id="confPassword"
                            type={values.showPassword ? 'text' : 'password'}
                            value={values.confPassword}
                            onChange={handleChange('confPassword')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        className={classes.visIcon}
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            labelWidth={70}
                        />
                        {values.errors && values.errors.confPassword && <FormHelperText>{(values.errors && values.errors.confPassword) ? values.errors.confPassword[0] : ''}</FormHelperText>}
                    </FormControl>
                </Grid>

                <Grid item xs={12}>
                    <FormControl className={classes.textField} fullWidth required variant="outlined" error={!!(values.errors && values.errors.terms)}>
                        <FormControlLabel
                            className={classes.controlLabel}
                            control={
                                <Checkbox
                                    onChange={handleChange('terms')}
                                    value="checkedTerms"
                                    color="primary"
                                    required
                                />
                            }
                            label={formInfo.text.terms}
                        />
                        {values.errors && values.errors.terms && <FormHelperText>{(values.errors && values.errors.terms) ? values.errors.terms[0] : ''}</FormHelperText>}
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                    <FormControl className={classes.textField} fullWidth required variant="outlined" error={!!(values.errors && values.errors.accept)}>
                        <FormControlLabel
                            className={classes.controlLabel}
                            control={
                                <Checkbox
                                    onChange={handleChange('accept')}
                                    value="checkedAccept"
                                    color="primary"
                                    required
                                />
                            }
                            label={formInfo.text.accept}
                        />
                        {values.errors && values.errors.accept && <FormHelperText>{(values.errors && values.errors.accept) ? values.errors.accept[0] : ''}</FormHelperText>}
                    </FormControl>
                </Grid>
                <Grid container alignContent="center" alignItems="center" justify="center" className={classes.btn}>
                    <Button variant="contained" color="primary" onClick={handleSubmitBtn}>Create Account</Button>
                </Grid>
            </Grid>
        </form>
    )
}

export default Form;
