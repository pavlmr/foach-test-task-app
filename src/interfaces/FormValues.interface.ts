export default interface Values {
    firstName: string;
    lastName: string;
    phone: string;
    amountInvest: string;
    email: string;
    confEmail: string;
    password: string;
    confPassword: string;
    terms: string;
    accept: string;
}