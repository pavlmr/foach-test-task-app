import Errors from './FormErrors.interface';
import Values from './FormValues.interface'

export default interface State extends Values {
    showPassword: boolean;
    errors: Errors | null;
}