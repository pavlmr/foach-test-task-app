export default interface Errors {
    firstName: [number],
    lastName: [number],
    phone: [number],
    amountInvest: [number],
    email: [number],
    confEmail: [number],
    password: [number],
    confPassword: [number],
    terms: [number],
    accept: [number]
}