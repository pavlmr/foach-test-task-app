import User from './User.interface'

export default interface State {
    data:{
        users: [User]
    }
}