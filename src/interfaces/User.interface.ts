export default interface User {
    name: string,
    avatar: string,
    position: string,
    vacation: boolean
}