import React, {FC} from 'react';
import { Grid, Typography } from '@material-ui/core'

const style = {
    fontSize: '3em',
    lineHeight: '1em'
}

const NotFound: FC = () => (
    <Grid container direction="row" >
        <Grid item xs>
            <Typography variant='h1' style={style}>Такой странице не существует</Typography>
            <Typography variant='subtitle1'>Ошибка 404</Typography>
        </Grid>
    </Grid>
)

export default NotFound;