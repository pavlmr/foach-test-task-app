import { combineReducers } from 'redux';
import data from './data';

const rootReducer = combineReducers({ data });

export type AppState = ReturnType < typeof rootReducer >

export default rootReducer;