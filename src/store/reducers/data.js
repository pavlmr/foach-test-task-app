import { LOAD_USERS } from '../types';

const initialState = {
    users: { }
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case LOAD_USERS:
            return {
                ...state,
                users: payload.items
            };
		default:
		    return state;
	}
};