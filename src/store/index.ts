import { getUsers } from './action';
import { rootReducer } from './reducers'

export {
    getUsers,
    rootReducer
}