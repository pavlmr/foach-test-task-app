import { LOAD_USERS } from '../types';
import { users } from '../../data';

export const getUsers = () => async (dispatch: any) => {
    const payload = users;

    payload && dispatch({
        type: LOAD_USERS,
        payload: payload
    })
}