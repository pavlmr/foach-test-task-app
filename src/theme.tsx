import red from '@material-ui/core/colors/red';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    breakpoints:{
        values: {
            xs: 0,
            sm: 570,
            md: 786,
            lg: 1024,
            xl: 1280
        }
    },
    palette: {
        primary: {
            main: '#1EAAFC',
        },
        secondary: {
            main: '#3AB047',
        },
        error: {
            main: red.A400,
        },
        background: {
            default: '#ECF6FE',
        },
    },
    typography: {
        fontSize: 14,
    },
    overrides: {
        MuiInputBase: {
            root: {
                fontFamily: 'Lato',
                color: '#161616'
            }
        },
        MuiTypography: {
            body1: {
                fontFamily: 'Lato',
                lineHeight: '20px',
                color: '#161616'
            }
        },
        MuiOutlinedInput:{
            root:{
                borderRadius: '0px',
                "&$focused $notchedOutline": {
                    borderWidth: "1px"
                },
                "&:hover $notchedOutline": {
                    borderColor: "#dddddd"
                }
            },
            notchedOutline:{
                borderColor: '#EFEFEF',
            },
            input: {
                paddingTop: '14px',
                paddingBottom: '14px',
                paddingLeft: '22px'
            }
        },
        MuiInputLabel: {
            outlined: {
                transform: 'translate(22px, 16px) scale(1)',
                "&$shrink": {
                    transform: 'translate(18px, -8px) scale(1)',
                    background: '#FFFFFF',
                    paddingLeft: '2px',
                    paddingRight: '2px'
                }
            },
            // shrink: {
            //     transform: 'translate(14px, -6px) scale(1)'
            // }
        },
        MuiFormLabel: {
            root: {
                fontSize: '14px',
                lineHeight: '17px',
                "&$focused": {
                    // background: '#FFFFFF',
                    // paddingLeft: '2px',
                    // paddingRight: '2px'
                }
            }
        },
        MuiButton: {
            root: {
                borderRadius: '0px',
                paddingTop: '16px',
                paddingBottom: '14px',
                letterSpacing: '3px',
                lineHeight: '17px'
            },
            contained: {
                textShadow: 'none',
                boxShadow: 'none'
            },
            containedPrimary:{
                color: '#FFFFFF'
            }
        },
        MuiIconButton: {
            label: {
                // color: '#EFEFEF'
            }
        },
        MuiCheckbox: {
            root:{
                marginTop: '-9px',
                color: '#EFEFEF'
            }
        }
    }
});

export default theme;