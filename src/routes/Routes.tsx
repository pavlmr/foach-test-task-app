import React, { FC } from 'react';
import { Route, Switch } from 'react-router-dom';
import SigntUp from '../pages/SigntUp';
import Users from '../pages/Users';
import NotFound from '../pages/NotFound'

interface IRoutesProps {};

const Routes: FC<IRoutesProps> = () => (
    <Switch>
        <Route path="/" exact component={SigntUp} />
        <Route path="/users" component={Users} />
        <Route path='*' component={NotFound} />
    </Switch>
)
export default Routes;