import amountToInvest from './amountToInvest';
import constraints from './constraints';
import formInfo from './formInfo';
import users from './users.json'

export{
    amountToInvest,
    constraints,
    formInfo,
    users
}