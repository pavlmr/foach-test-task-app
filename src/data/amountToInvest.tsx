const amountToInvest = [
    {value: '10',
    label: 'to $10'},
    {value: '10-100',
    label: 'to $10 from $100'},
    {value: '100-1000',
    label: 'to $100 from $1000'},
    {value: '1000',
    label: 'from $1000'},
]

export default amountToInvest