interface formInfo {
    text: {
        terms: string,
        accept: string
    }
}

const formInfo: formInfo = {
    text: {
        terms: 'I certify that I am 18 years of age or older, and I agree to the Terms of Service and Privacy Policy.',
        accept: 'I would like to receive important information and periodic news updates.',
    }
}

export default formInfo;