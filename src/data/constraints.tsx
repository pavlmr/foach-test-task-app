const constraints = {
    firstName: {
        presence: {
            allowEmpty: false
        },
        length: {
            minimum: 2,
            maximum: 12,
            message: "must be at least 2 characters"
        },

        type: 'string'
    },

    lastName: {
        length: {
            minimum: 2,
            maximum: 12,
            message: "must be at least 2 characters"
        },
        presence: {
            allowEmpty: false
        },

        type: 'string'
    },
    phone: {
        length: {
            minimum: 2,
            maximum: 12,
            message: "must be at least 2 characters"
        },
        presence: {
            allowEmpty: false
        },

        type: 'string'
    },
    amountInvest: {
        presence: {
            allowEmpty: false
        }
    },

    email: {
        email: {
            message: '^E-mail address is invalid'
        },

        presence: {
            allowEmpty: false
        },

        type: 'string'
    },

    confEmail: {
        email: {
            message: '^E-mail address confirmation is invalid'
        },

        equality: {
            attribute: 'email',
            message: '^E-mail address confirmation is not equal to e-mail address'
        },

        presence: {
            allowEmpty: false
        },

        type: 'string'
    },

    password: {
        length: {
            minimum: 8
        },

        presence: {
            allowEmpty: false
        },

        type: 'string'
    },

    confPassword: {
        equality: 'password',

        length: {
            minimum: 8
        },

        presence: {
            allowEmpty: false
        },

        type: 'string'
    },
    terms: {
        presence: true,
        inclusion: {
            within: ['checkedTerms'],
            message: "must be a checked"
        },
        type: 'string'

    },

    accept: {
        presence: {
            allowEmpty: false
        },
        inclusion: {
            within: ['checkedAccept'],
            message: "must be a checked"
        },
        type: 'string'
    },
};

export default constraints;