import React, { FC, useEffect } from 'react';
import { Router } from 'react-router-dom';
import Routes from './routes/Routes'
import { createBrowserHistory } from "history";
import Container from '@material-ui/core/Container';

import { useDispatch } from "react-redux";
import { getUsers } from './store';

const history = createBrowserHistory();

const style = {
  minHeight: '100vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  paddingLeft: '20px',
  paddingRight: '20px'
}

const App: FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);
  
  return (
      <Container maxWidth="lg" style={style} >
       <Router history={history}>
          <Routes /> 
        </Router>
      </Container>
  )}

export default App;
